import { Injectable } from "@angular/core";
import {
  LocalNotifications,
  ELocalNotificationTriggerUnit
} from "@ionic-native/local-notifications/ngx";

@Injectable({
  providedIn: "root"
})
export class NotificationsService {
  constructor(private localNotifications: LocalNotifications,) {}
  public notificationIds: Array<number> =[
    90, 91, 92, 93, 94, 95, 96
  ];

  notifyCleanse(){
    this.localNotifications.schedule({
      id: 90,
      title: "Wake Up Slowly",
      text:
        "Have a cup of warm water with lemon or some of your favourite herbal tea. Do 15 minutes of meditation to check in with yourself. Say the chakra affirmations.",
      trigger: { every: { hour: 7, minute: 45 } }
    });
    this.localNotifications.schedule({
      id: 91,
      title: "First Serving",
      text: "8:00",
      trigger: { every: { hour: 8, minute: 0 } }
    });
    this.localNotifications.schedule({
      id: 92,
      title: "Second Serving",
      text: "10:00",
      trigger: { every: { hour: 10, minute: 0 } }
    });
    this.localNotifications.schedule({
      id: 93,
      title: "Third Serving",
      text: "12:00",
      trigger: { every: { hour: 12, minute: 0 } }
    });
    this.localNotifications.schedule({
      id: 94,
      title: "Fourth Serving",
      text: "14:00",
      trigger: { every: { hour: 14, minute: 0 } }
    });
    this.localNotifications.schedule({
      id: 95,
      title: "Fifth Serving",
      text: "16:00",
      trigger: { every: { hour: 16, minute: 0 } }
    });
    this.localNotifications.schedule({
      id: 96,
      title: "Final Serving",
      text: "18:00",
      trigger: { every: { hour: 18, minute: 0 } }
    });
}
  clearNotifications(){
      this.localNotifications.cancel(this.notificationIds);
  }
//   clearDailyNotification(){
//     this.localNotifications.isTriggered(97).then(response =>{
//       if(response == true){
//         this.clearNotifications();
//       }
//     }).catch(err => console.log(err))
   
// }
}