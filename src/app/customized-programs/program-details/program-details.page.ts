import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

import { ShopifyService } from '../../services/shopify.service';

@Component({
  selector: 'app-program-details',
  templateUrl: './program-details.page.html',
  styleUrls: ['./program-details.page.scss'],
})
export class ProgramDetailsPage implements OnInit {
  minDate;
  startDate;
  public days: any;

  constructor(
    private shopifyService: ShopifyService,
    private navCtrl: NavController,
    private storage: Storage
  ) { 
    this.days = [
      {
        day: "ONE",
        dayNum: 1,
        class: "border",
      },
      {
        day: "THREE",
        dayNum: 3,
        class: "border",
      },
      {
        day: "FIVE",
        dayNum: 5,
        class: "border",
      },
      {
        day: "SEVEN",
        dayNum: 7,
        class: "border",
      },
      {
        day: "TEN",
        dayNum: 10,
        class: "border",
      },
    ];
  }

  ngOnInit() {
    let date = new Date();
    date.setDate(date.getDate() + 2);
    this.minDate = this.formatDate(date);
  }

  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  setNumDays(num) {
    this.shopifyService.numDays = parseInt(num);
    this.storage.set('numDays', parseInt(num));
  }

  navCreate() {
    this.navCtrl.navigateForward("create-plan");
  }

  itemClicked(icon, icons){
    icons.forEach(entry => {
      if(entry == icon){
        icon.class = "border-active";
      } else {
        entry.class = "border";
      }
    });
    this.setNumDays(icon.dayNum);
  }
}
