import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TabsPageRoutingModule } from './tabs.router.module';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      { path: 'explore', loadChildren: '../explore/explore.module#ExplorePageModule'},
      { path: 'track-progress', loadChildren: '../track-progress/track-progress.module#TrackProgressPageModule'},
      { path: 'cart', loadChildren: '../cart/cart.module#CartPageModule'},
      { path: 'store', loadChildren: '../store/store.module#StorePageModule' }
    ]
  },
  {
    path:'',
    redirectTo: '/tabs/store',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TabsPageRoutingModule
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
