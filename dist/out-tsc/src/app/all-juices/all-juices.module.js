import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AllJuicesPage } from './all-juices.page';
var routes = [
    {
        path: '',
        component: AllJuicesPage
    }
];
var AllJuicesPageModule = /** @class */ (function () {
    function AllJuicesPageModule() {
    }
    AllJuicesPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [AllJuicesPage]
        })
    ], AllJuicesPageModule);
    return AllJuicesPageModule;
}());
export { AllJuicesPageModule };
//# sourceMappingURL=all-juices.module.js.map