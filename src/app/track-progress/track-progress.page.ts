import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, LoadingController, AlertController } from '@ionic/angular';

import { NotificationsService } from '../services/notifications.service';

@Component({
  selector: 'app-track-progress',
  templateUrl: './track-progress.page.html',
  styleUrls: ['./track-progress.page.scss'],
})
export class TrackProgressPage {
  happiness: number;
  energy: number;
  digestion: number;
  entries1: any;
  entries2: any;
  public endOfDay: boolean = false;
  public progress: number = 0;
  public percentage: String;
  public hapMessage: String;
  public engMessage: String;
  public digMessage: String;

  public displayRecieve: boolean = true;
  public displayCancel: boolean = false;

  constructor(
    private navCtrl: NavController,
    private storage: Storage,
    private loadingController: LoadingController,
    private localNotifications: NotificationsService,
    private alertCtrl: AlertController
  ) { 
    this.reset();
  }

  reset() {
    this.entries1 = [
      {
        time: "8am",
        icon: "square-outline",
      },
      {
        time: "10am",
        icon: "square-outline",
      },
      {
        time: "12pm",
        icon: "square-outline",
      },
    ]
    this.entries2 = [
      {
        time: "3pm",
        icon: "square-outline",
      },
      {
        time: "5pm",
        icon: "square-outline",
      },
      {
        time: "7pm",
        icon: "square-outline",
      }
    ];
  }

  summary() {
    let count = 0;
    
    this.entries1.forEach(item => {
      if(item.icon == "checkbox-outline") {
        count++;
      }
    });

    this.entries2.forEach(item => {
      if(item.icon == "checkbox-outline") {
        count++;
      }
    });

    this.progress = count/6;
    this.percentage = String(Math.round((this.progress * 100)));

    if(this.happiness < 5) {
      this.hapMessage = "Sorry you’re feeling down. Try taking a walk in nature, soak in the bath or any other form of self-care you can offer yourself right now. You are worthy of your own love.";
    }
    else{
      this.hapMessage = "Woop Woop, we’re so happy you’re happy! Just remember, slow and steady wins the race, so stay soft, supple and keep glowing and flowing baby!"
    }

    if(this.energy < 5) {
      this.engMessage = " If you’re finding yourself low on energy try and make more time for yourself to rest and restore. You’re also welcome to add additional juices/broths to your program, we like to look at this process more of a juice feast than a juice fast :)"
    }
    else{
      this.engMessage = "You’re flying baby! Keep those tail-feathers fluffed and frisky."
    }

    if(this.digestion < 5) {
      this.digMessage = "It’s quite normal for your digestion to change a bit in the first few days of a cleanse. Try introducing some more soothing liquids like cucumber juice, vegan broth or aloe-vera juice to soothe and soften your tummy."
    }
    else{
      this.digMessage = "Stomach of steel! Well done, your micro biome is thriving with the addition of all those live prebiotics <3"
    }
    
    this.endOfDay = true;
    this.happiness = 0;
    this.energy = 0;
    this.digestion = 0;
    this.reset();
  }

  click(t, a){
    a.forEach(entry => {
      if(entry.time == String(t)){
        if(entry.icon == "checkbox-outline") {
          entry.icon = "square-outline";
        } else {
          entry.icon = "checkbox-outline";
        }
      }
    });
  }

  async presentAlertEnabled() {
    const alert = await this.alertCtrl.create({
      header: "Alert",
      subHeader: "Reminders Enabled",
      message: "You will recieve daily reminders for your cleanses.",
      buttons: ["OK"]
    });
    await alert.present();
  }

  async presentAlertDisabled() {
    const alert = await this.alertCtrl.create({
      header: "Alert",
      subHeader: "Reminders Disabled",
      message: "You will not recieve reminders.",
      buttons: ["OK"]
    });

    await alert.present();
  }

  enableNotifications() {
    this.localNotifications.notifyCleanse();
    this.displayRecieve = false;
    this.displayCancel = true;
    this.presentAlertEnabled();
  }

  disableNotifications() {
    this.localNotifications.clearNotifications();
    this.displayRecieve = true;
    this.displayCancel = false;
    this.presentAlertDisabled();
  }
}
