import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular'
import { LoadingController } from '@ionic/angular'
import { Storage } from '@ionic/storage';

import { ShopifyService } from '../../services/shopify.service';

@Component({
  selector: 'app-nourishd-cleanses',
  templateUrl: './nourishd-cleanses.page.html',
  styleUrls: ['./nourishd-cleanses.page.scss'],
})
export class NourishdCleansesPage implements OnInit {

  public cleanses: any = [];

  constructor(
    private navCtrl: NavController,
    private shopifyService: ShopifyService,
    private loadingController: LoadingController,
    private storage: Storage
  ) {
    const allCleanseID = 'Z2lkOi8vc2hvcGlmeS9Db2xsZWN0aW9uLzkwNDY1MjcxOTA2';

    this.shopifyService.client.collection.fetchWithProducts(allCleanseID).then((collection) => {
      this.cleanses = collection.products;
    });
  }

  ngOnInit() {
  }

  navToCleanse(id) {
    this.storage.set('id', id);
    this.navCtrl.navigateForward('choose-cleanse');
  }

  ionViewDidEnter(){
    this.presentLoading();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: '',
      duration: 500
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 5000,
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }
}
